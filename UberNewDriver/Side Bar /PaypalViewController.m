//
//  PaypalViewController.m
//  Kartrip Driver
//
//  Created by My Mac on 15/03/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import "PaypalViewController.h"

@interface PaypalViewController ()

@end

@implementation PaypalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.lblConnect setTitle:NSLocalizedString(@"CONNECT", nil) forState:UIControlStateNormal];
    [self.lblConnect setTitle:NSLocalizedString(@"CONNECT", nil) forState:UIControlStateSelected];
    [self getPaypalEmail];
    [self.viewForEmailPaypal setHidden:YES];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getPaypalEmail
{
    if([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:@"Please wait..."];
        NSString *strUserId = [PREF objectForKey:@"id"];
        NSString *strUserToken = [PREF objectForKey:@"token"];
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GETPAYPAL_ID,PARAM_ID,strUserId,PARAM_TOKEN,strUserToken];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 [APPDELEGATE hideLoadingView];
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSString *str = [response valueForKey:@"paypal_email"];
                     
                     if(str.length>1)
                     {
                         self.lblPaypal.text = str;
                         [self.lblConnect setTitle:NSLocalizedString(@"EDIT", nil) forState:UIControlStateNormal];
                         [self.lblConnect setTitle:NSLocalizedString(@"EDIT", nil) forState:UIControlStateSelected];
                     }
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     
                     if([str isEqualToString:@"26"])
                     {
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                     else
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                         
                     }
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (IBAction)backBtnPressed:(id)sender
{
    [self.view endEditing:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)onClickOK:(id)sender
{
    if(self.txtEmail.text.length>1)
    {
        if([APPDELEGATE connected])
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
            
            NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
            
            NSString *strUserId=[PREF objectForKey:PREF_USER_ID];
            NSString *strUserToken=[PREF objectForKey:PREF_USER_TOKEN];
            
            [dictparam setObject:strUserId forKey:PARAM_ID];
            [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
            [dictparam setObject:self.txtEmail.text forKey:@"email"];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CONNECT_PAYPAL withParamData:dictparam withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         [APPDELEGATE showToastMessage:@"You have added your email address"];
                         [self.view endEditing:YES];
                         [self.viewForEmailPaypal setHidden:YES];
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                     else
                     {
                         NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                         
                         if([str isEqualToString:@"26"])
                         {
                             [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                         }
                     }
                 }
             }];
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Please enter your email address" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)onClickConnect:(id)sender
{
    [self.viewForEmailPaypal setHidden:NO];
}

@end