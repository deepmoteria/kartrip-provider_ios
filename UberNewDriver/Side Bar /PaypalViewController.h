//
//  PaypalViewController.h
//  Kartrip Driver
//
//  Created by My Mac on 15/03/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import "BaseVC.h"

@interface PaypalViewController : BaseVC
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)onClickOK:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
- (IBAction)onClickConnect:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForEmailPaypal;
@property (weak, nonatomic) IBOutlet UILabel *lblPaypal;
@property (weak, nonatomic) IBOutlet UIButton *lblConnect;

@end
