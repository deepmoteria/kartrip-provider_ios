//  SideBarVC.m
//  UberNewDriver
//
//  Created by Deep Gami on 27/09/14.
//  Copyright (c) 2014 Deep Gami. All rights reserved.

#import "SideBarVC.h"
#import "SWRevealViewController.h"
#import "PickMeUpMapVC.h"
#import "CellSlider.h"
#import "UIView+Utils.h"
#import "UIImageView+Download.h"
#import "CustomAlert.h"
#import "NSBundle+Language.h"

@interface SideBarVC ()
{
    NSMutableArray *arrImages,*arrListName,*arrIdentifire;
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
}

@end

@implementation SideBarVC

@synthesize ViewObj;


#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self.view setBackgroundColor:[UIColor clearColor]];
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.contentInset = UIEdgeInsetsZero;
    arrListName=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"PROFILE",nil),NSLocalizedString(@"History",nil),NSLocalizedString(@"Share",nil),nil];
    
    arrIdentifire=[[NSMutableArray alloc] initWithObjects:SEGUE_PROFILE,SEGUE_HISTORY,SEGUE_SHARE,nil];
    
    arrImages=[[NSMutableArray alloc]initWithObjects:@"nav_profile",@"ub__nav_history",@"nav_referral",@"nav_share",nil];
    
    [self getPagesData];
    internet=[APPDELEGATE connected];
}

-(void)viewWillAppear:(BOOL)animated
{
    strUserId=[PREF objectForKey:PREF_USER_ID];
    strUserToken=[PREF objectForKey:PREF_USER_TOKEN];
    
    [self.imgProfilePic applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    [self.imgProfilePic downloadFromURL:[arrUser valueForKey:PREF_USER_PICTURE] withPlaceholder:nil];
    self.lblName.font=[UberStyleGuide fontRegularBold:18.0f];
    self.lblSoundStatus.font = [UberStyleGuide fontRegularBold:18.0f];
    self.lblName.text=[NSString stringWithFormat:@"%@ %@",[arrUser valueForKey:@"first_name"],[arrUser valueForKey:@"last_name"]];
    
    self.navigationItem.leftBarButtonItem=nil;
    
    self.tableView.backgroundView=nil;
    self.tableView.backgroundColor=[UIColor clearColor];
    
    NSString *strSoundStatus = [PREF objectForKey:@"SoundStatus"];
    
    if ([[PREF valueForKey:@"SOUND"] isEqualToString:@"on"])
    {
        [self.bntSound setImage:[UIImage imageNamed:@"soundOn"] forState:UIControlStateNormal];
        self.lblSoundStatus.text = NSLocalizedString(@"ON", nil);
    }
    else
    {
        [self.bntSound setImage:[UIImage imageNamed:@"soundOff"] forState:UIControlStateNormal];
        self.lblSoundStatus.text = NSLocalizedString(@"OFF", nil);
    }
}

-(void)getPagesData
{
    if([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:@"Please wait..."];
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@",FILE_PAGES,PARAM_ID,strUserId];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"Page Data= %@",response);
             //[APPDELEGATE hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     arrPage = [[NSMutableArray alloc]init];
                     arrPage=[response valueForKey:@"informations"];
                     NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                     NSMutableArray *arrImg=[[NSMutableArray alloc]init];
                     for (int i=0; i<arrPage.count; i++)
                     {
                         NSMutableDictionary *temp1=[arrPage objectAtIndex:i];
                         [arrTemp addObject:[temp1 valueForKey:@"title"]];
                         [arrImg addObject:@"nav_support"];
                     }
                     
                     [arrListName addObjectsFromArray:arrTemp];
                     [arrIdentifire addObjectsFromArray:arrTemp];
                     
                     [arrImages addObjectsFromArray:arrImg];
                     
                     //[arrListName addObject:NSLocalizedString(@"Change Language", nil)];
                     
                     [arrImages addObject:@"nav_referral"];
                     
                     [arrListName addObject:NSLocalizedString(@"LOG OUT", nil)];
                     [arrImages addObject:@"ub__nav_logout"];
                     
                     [self.tableView reloadData];
                 }
             }
             
         }];
        
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - TableView Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrListName.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellSlider *cell=(CellSlider *)[tableView dequeueReusableCellWithIdentifier:@"CellSlider"];
    if (cell==nil) {
        cell=[[CellSlider alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellSlider"];
    }
    cell.lblName.font=[UberStyleGuide fontRegular:15.43f];
    cell.lblName.text=[arrListName objectAtIndex:indexPath.row];
    cell.imgIcon.image=[UIImage imageNamed:[arrImages objectAtIndex:indexPath.row]];
    
    //[cell setCellData:[arrSlider objectAtIndex:indexPath.row] withParent:self];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSLog(@"%ld",(long)indexPath.row);
    if ([[arrListName objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"LOG OUT", nil)])
    {
        /*
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"LOG OUT", nil)  message:NSLocalizedString(@"Are Sure You want to log Out", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
         alert.tag=100;
         [alert show];
         */
        CustomAlert *alert = [[CustomAlert alloc] initWithTitle:NSLocalizedString(@"LOG OUT", nil) message:NSLocalizedString(@"Are Sure You want to log Out", nil) delegate:self cancelButtonTitle:@"" otherButtonTitle:@""];
        // alert.tag=100;
        // [self.view setUserInteractionEnabled:FALSE];
        //[alert showInView:self.view];
        
        [APPDELEGATE.window addSubview:alert];
        [APPDELEGATE.window bringSubviewToFront:alert];
        [self.revealViewController rightRevealToggle:self];
        
        
        return;
    }
    if ([[arrListName objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"Share", nil)])
    {
        NSLog(@"shareButton pressed");
        
        NSString *texttoshare = @"Hello"; //this is your text string to share
        //UIImage *imagetoshare = @""; //this is your image to share
        NSArray *activityItems = @[texttoshare];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
        [self presentViewController:activityVC animated:TRUE completion:nil];
        
        return;
    }
    /*if([[arrListName objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"Change Language", nil)])
    {
        [self onclickChangeLanguage:nil];
        return;
    }*/
    if ((indexPath.row >2)&&(indexPath.row<(arrListName.count-1)))
    {
        [self.revealViewController rightRevealToggle:self];
        
        UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
        
        ViewObj=(PickMeUpMapVC *)[nav.childViewControllers objectAtIndex:0];
        
        NSDictionary *dictTemp=[arrPage objectAtIndex:indexPath.row-3];
        
        [ViewObj performSegueWithIdentifier:@"contact us" sender:dictTemp];
        return;
    }
    [self.revealViewController rightRevealToggle:self];
    
    UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
    
    ViewObj=(PickMeUpMapVC *)[nav.childViewControllers objectAtIndex:0];
    
    if(ViewObj!=nil)
    {
        [ViewObj performSegueWithIdentifier:[arrIdentifire objectAtIndex:indexPath.row] sender:self];
    }
    //[ViewObj goToSetting:[[arrListName objectAtIndex:indexPath.row] lowercaseString]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

/*-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
 {
 UIView *viewFooter=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
 UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(0, 43, tableView.frame.size.width, 1)];
 //viewLine.backgroundColor=[UIColor whiteColor];
 UIButton *v=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
 [v addTarget:self action:@selector(onclickChangeLanguage:) forControlEvents:UIControlEventTouchUpInside];
 
 v.titleLabel.font = [UberStyleGuide fontRegular:15.43f];
 [v setTitle:@"Change Language" forState:UIControlStateNormal];
 [v setTitle:@"Change Language" forState:UIControlStateHighlighted];
 [v setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 [v setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
 v.backgroundColor=[UIColor clearColor];
 [viewFooter addSubview:v];
 //[viewFooter addSubview:viewLine];
 return viewFooter;
 }*/

#pragma mark -
#pragma mark- UIButton Action methods

-(IBAction)onclickChangeLanguage:(id)sender
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Select Your Language", nil) message:nil delegate:self cancelButtonTitle:@"English" otherButtonTitles:@"Español", nil];
    alert.tag=200;
    [alert show];
}

#pragma mark - Alert Button Clicked Event


- (void)customAlertView:(CustomAlert*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self.view setUserInteractionEnabled:YES];
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"WAITING_LOGOUT", nil)];
        
        if([APPDELEGATE connected])
        {
            NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
            
            [dictparam setObject:strUserId forKey:PARAM_ID];
            [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_LOGOUT withParamData:dictparam withBlock:^(id response, NSError *error)
             {
                 
                 NSLog(@"Log Out= %@",response);
                 [APPDELEGATE hideLoadingView];
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         
                         [PREF removeObjectForKey:PARAM_REQUEST_ID];
                         [PREF removeObjectForKey:PARAM_SOCIAL_ID];
                         [PREF removeObjectForKey:PREF_EMAIL];
                         [PREF removeObjectForKey:PREF_LOGIN_BY];
                         [PREF removeObjectForKey:PREF_PASSWORD];
                         [PREF removeObjectForKey:PREF_USER_ID];
                         [PREF removeObjectForKey:PREF_USER_TOKEN];
                         [PREF setBool:NO forKey:PREF_IS_LOGIN];
                         
                         [PREF setObject:@"" forKey:PREF_USER_ID];
                         [PREF setObject:@"" forKey:PREF_USER_TOKEN];
                         [PREF synchronize];
                         
                         
                         
                         //                                 if ([self.delegate respondsToSelector:@selector(invalidateTimer)])
                         //                                 {
                         //                                     [self.delegate invalidateTimer];
                         //                                 }
                         
                         
                         [self.navigationController   popToRootViewControllerAnimated:YES];
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"LOGED_OUT", nil)];
                         [alertView removeFromSuperview];
                     }
                 }
                 
             }];
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if (buttonIndex == 1)
    {
        [self.view setUserInteractionEnabled:YES];
        //[alertView removeFromSuperview];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==200)
    {
        if (buttonIndex==0)
        {
            [NSBundle setLanguage:@"en"];
            [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"User_language"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLaunch"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            NSString *storyboardName = @"Main_iPhone"; // Your storyboard name
            UIStoryboard *storybaord = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
            delegate.window.rootViewController = [storybaord instantiateInitialViewController];
        }
        else
        {
            [NSBundle setLanguage:@"es"];
            [[NSUserDefaults standardUserDefaults] setObject:@"es" forKey:@"User_language"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLaunch"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            NSString *storyboardName = @"Main_iPhone"; // Your storyboard name
            UIStoryboard *storybaord = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
            delegate.window.rootViewController = [storybaord instantiateInitialViewController];
        }
    }
    /*if (buttonIndex == 1)
     {
     [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"WAITING_LOGOUT", nil)];
     
     
     if([APPDELEGATE connected])
     {
     
     NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
     
     [dictparam setObject:strUserId forKey:PARAM_ID];
     [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
     
     AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
     [afn getDataFromPath:FILE_LOGOUT withParamData:dictparam withBlock:^(id response, NSError *error)
     {
     
     NSLog(@"Log Out= %@",response);
     [APPDELEGATE hideLoadingView];
     if (response)
     {
     if([[response valueForKey:@"success"] intValue]==1)
     {
     [PREF removeObjectForKey:PARAM_REQUEST_ID];
     [PREF removeObjectForKey:PARAM_SOCIAL_ID];
     [PREF removeObjectForKey:PREF_EMAIL];
     [PREF removeObjectForKey:PREF_LOGIN_BY];
     [PREF removeObjectForKey:PREF_PASSWORD];
     [PREF removeObjectForKey:PREF_USER_ID];
     [PREF removeObjectForKey:PREF_USER_TOKEN];
     [PREF setBool:NO forKey:PREF_IS_LOGIN];
     
     [PREF setObject:@"" forKey:PREF_USER_ID];
     [PREF setObject:@"" forKey:PREF_USER_TOKEN];
     [PREF synchronize];
     
     [self.navigationController popToRootViewControllerAnimated:YES];
     [APPDELEGATE showToastMessage:NSLocalizedString(@"LOGED_OUT", nil)];
     
     }
     }
     
     }];
     
     }
     else
     {
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
     [alert show];
     }
     
     
     }*/
    
}

#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark
#pragma mark - sound button click event

- (IBAction)pressSoundBtn:(id)sender
{
    
    NSString *strSoundStatus = [PREF objectForKey:@"SoundStatus"];
    
    if ([strSoundStatus isEqualToString:NSLocalizedString(@"ON", nil)])
    {
        [self.bntSound setImage:[UIImage imageNamed:@"soundOff"] forState:UIControlStateNormal];
        self.lblSoundStatus.text = NSLocalizedString(@"OFF", nil);
        [PREF setObject:@"off" forKey:@"SOUND"];
        [PREF synchronize];
    }
    else
    {
        [self.bntSound setImage:[UIImage imageNamed:@"soundOn"] forState:UIControlStateNormal];
        self.lblSoundStatus.text = NSLocalizedString(@"ON", nil);
        [PREF setObject:@"on" forKey:@"SOUND"];
        [PREF synchronize];
    }
    [PREF setObject:self.lblSoundStatus.text forKey:@"SoundStatus"];
    [PREF synchronize];
}

@end