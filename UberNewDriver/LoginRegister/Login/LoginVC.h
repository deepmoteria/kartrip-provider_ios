//
//  LoginVC.h
//  UberNewDriver
//
//  Created by Deep Gami on 27/09/14.
//  Copyright (c) 2014 Deep Gami. All rights reserved.
//

#import "BaseVC.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>

@interface LoginVC : BaseVC <UITextFieldDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewForForgotPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtForgotEmail;

- (IBAction)onClickSignIn:(id)sender;
- (IBAction)googleBtnPressed:(id)sender;
- (IBAction)facebookBtnPressed:(id)sender;
- (IBAction)forgotBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)onClickOkForgotPwd:(id)sender;

@property (nonatomic, retain) GTMOAuth2Authentication *auth;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property(nonatomic,weak)IBOutlet UIScrollView *scrLogin;

@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPsw;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;

@property (strong, nonatomic) IBOutlet UILabel *lblPassword;
@property (strong, nonatomic) IBOutlet UILabel *lblUsername;
@property (strong, nonatomic) IBOutlet UILabel *lblSignInWih;
@property (strong, nonatomic) IBOutlet UILabel *lblOr;

@end
